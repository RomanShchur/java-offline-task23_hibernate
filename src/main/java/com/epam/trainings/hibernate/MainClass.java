package com.epam.trainings.hibernate;

import com.epam.trainings.hibernate.model.Factory;
import com.epam.trainings.hibernate.model.tables.Car;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class MainClass {
  public static void main(String[] args) throws SQLException {
    Collection cars = Factory.getInstance().getCarDao().findAll();
    Iterator iterator = cars.iterator();
    System.out.println("======== All cars =========");
    while (iterator.hasNext()) {
      Car car = (Car) iterator.next();
      System.out.println(car.toString());
    }
  }
}
