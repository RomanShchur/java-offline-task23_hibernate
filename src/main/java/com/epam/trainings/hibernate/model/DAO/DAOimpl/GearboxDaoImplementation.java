package com.epam.trainings.hibernate.model.DAO.DAOimpl;

import com.epam.trainings.hibernate.model.DAO.GearboxDao;
import com.epam.trainings.hibernate.model.databaseConnection.HibernateUtil;
import com.epam.trainings.hibernate.model.tables.Gearbox;
import org.hibernate.Session;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GearboxDaoImplementation implements GearboxDao {
  public List<Gearbox> findAll() throws SQLException {
    Session session = null;
    List gearboxes = new ArrayList<Gearbox>();
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      gearboxes = session.createCriteria(Gearbox.class).list();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findAll'",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return gearboxes;
  }

  public Gearbox findById(Integer id) throws SQLException {
    Session session = null;
    Gearbox gearbox = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      gearbox = (Gearbox) session.load(Gearbox.class, id);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findById'"
        , JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return gearbox;
  }

  public void create(Gearbox gearbox) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.save(gearbox);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on insert ", JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void update(Gearbox gearbox, Integer id) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.update(gearbox);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on update",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void delete(Gearbox gearbox) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.delete(gearbox);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on delete",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public List<Gearbox> fidIsMechanical(Boolean isMechanical) throws SQLException {
    Session session = null;
    List gearboxes = new ArrayList<Gearbox>();
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      gearboxes = session.createCriteria(Gearbox.class).list();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findAll'",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return gearboxes;
  }
}
