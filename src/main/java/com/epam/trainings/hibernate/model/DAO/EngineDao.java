package com.epam.trainings.hibernate.model.DAO;


import com.epam.trainings.hibernate.model.tables.Engine;

import java.sql.SQLException;
import java.util.List;

public interface EngineDao extends MainDao<Engine, Integer> {
  Engine fidByFuelType(String fuelType) throws SQLException;
}
