package com.epam.trainings.hibernate.model.DAO;

import com.epam.trainings.hibernate.model.tables.Car;

import java.sql.SQLException;
import java.util.List;

public interface MainDao<T, ID> {
  List<T> findAll() throws SQLException;

  T findById(Integer id) throws SQLException;

  void create(T t) throws SQLException;

  void update(T entity, Integer id) throws SQLException;

  void delete(T t) throws SQLException;
}
