package com.epam.trainings.hibernate.model.tables;

import org.hibernate.annotations.Entity;
import org.hibernate.annotations.Table;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Table(appliesTo = "car_has_destributor")
public class CarDestributor {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "destributor_id", nullable = false)
  int destributorId;
  @Column(name = "car_car_stats", nullable = false)
  int carStats;
  @Column(name = "car_id", nullable = false)
  int carId;
  @Column(name = "car_manufacturer_id", nullable = false)
  int carManufacturer;
  @Column(name = "price", nullable = false)
  int price;

  public CarDestributor() { }

  public int getDestributorId() {
    return destributorId;
  }

  public void setDestributorId(int destributorId) {
    this.destributorId = destributorId;
  }

  public int getCarStats() {
    return carStats;
  }

  public void setCarStats(int carStats) {
    this.carStats = carStats;
  }

  public int getCarId() {
    return carId;
  }

  public void setCarId(int carId) {
    this.carId = carId;
  }

  public int getCarManufacturer() {
    return carManufacturer;
  }

  public void setCarManufacturer(int carManufacturer) {
    this.carManufacturer = carManufacturer;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  @Override public String toString() {
    return "CarDestributor{" + "price=" + price + ", destributorId="
      + destributorId + '}';
  }
}
