package com.epam.trainings.hibernate.model.tables;


import org.hibernate.annotations.Entity;
import org.hibernate.annotations.Table;
import org.jboss.logging.Field;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Table(appliesTo = "used_car")
public class UsedCar {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  int id;
  @Column(name = "celler_phone", nullable = false, length = 45)
  String cellerPhone;
  @Column(name = "price", nullable = false)
  int price;
  @Column(name = "user_id", nullable = false)
  int userId;
  @Column(name = "car_car_stats_id", nullable = false)
  int carCarStatsId;
  @Column(name = "city", nullable = false,length = 45)
  String city;
  @Column(name = "mileage", nullable = false)
  int mileage;
  @Column(name = "car_id", nullable = false)
  int carId;
  @Column(name = "car_manufacturer", nullable = false)
  int carManufacturer;

  public UsedCar() {
  }

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getCellerPhone() {
    return cellerPhone;
  }
  public void setCellerPhone(String cellerPhone) {
    this.cellerPhone = cellerPhone;
  }
  public int getPrice() {
    return price;
  }
  public void setPrice(int price) {
    this.price = price;
  }
  public int getMileage() {
    return mileage;
  }
  public void setMileage(int mileage) {
    this.mileage = mileage;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getCarCarStatsId() {
    return carCarStatsId;
  }

  public void setCarCarStatsId(int carCarStatsId) {
    this.carCarStatsId = carCarStatsId;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public int getCarId() {
    return carId;
  }

  public void setCarId(int carId) {
    this.carId = carId;
  }

  public int getCarManufacturer() {
    return carManufacturer;
  }

  public void setCarManufacturer(int carManufacturer) {
    this.carManufacturer = carManufacturer;
  }

  @Override public String toString() {
    return "UsedCar{" + "id=" + id + ", cellerPhone='" + cellerPhone + '\''
      + ", price=" + price + ", userId=" + userId + ", carCarStatsId="
      + carCarStatsId + ", city='" + city + '\'' + ", mileage=" + mileage
      + ", carId=" + carId + ", carManufacturer=" + carManufacturer + '}';
  }
}
