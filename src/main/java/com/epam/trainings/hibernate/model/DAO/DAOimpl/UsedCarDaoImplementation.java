package com.epam.trainings.hibernate.model.DAO.DAOimpl;

import com.epam.trainings.hibernate.model.databaseConnection.HibernateUtil;
import com.epam.trainings.hibernate.model.tables.UsedCar;
import org.hibernate.Session;

import javax.swing.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsedCarDaoImplementation {
  public List<UsedCar> findAll() throws SQLException {
    Session session = null;
    List usedCar = new ArrayList<UsedCar>();
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      usedCar = session.createCriteria(UsedCar.class).list();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findAll'",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return usedCar;
  }

  public UsedCar findById(Integer id) throws SQLException {
    Session session = null;
    UsedCar usedCar = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      usedCar = (UsedCar) session.load(UsedCar.class, id);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findById'"
        , JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return usedCar;
  }

  public void create(UsedCar usedCar) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.save(usedCar);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on insert ", JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void update(UsedCar usedCar, Integer id) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.update(usedCar);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on update",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void delete(UsedCar usedCar) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.delete(usedCar);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on delete",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }}
