package com.epam.trainings.hibernate.model.DAO.DAOimpl;

import com.epam.trainings.hibernate.model.DAO.CarDao;
import com.epam.trainings.hibernate.model.databaseConnection.HibernateUtil;
import com.epam.trainings.hibernate.model.tables.Car;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarDaoImplementation implements CarDao {
  public List<Car> findAll() throws SQLException {
    Session session = null;
    List cars = new ArrayList<Car>();
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      CriteriaBuilder builder = session.getCriteriaBuilder();
      CriteriaQuery<Car> criteria = builder.createQuery(Car.class);
      Root<Car> contactRoot = criteria.from(Car.class);
      criteria.select(contactRoot);
      cars = session.createQuery(criteria).getResultList();
//      cars = session.createCriteria(Car.class).list();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findAll'",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return cars;
  }

  public Car findById(Integer id) throws SQLException {
    Session session = null;
    Car car = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      car = (Car) session.load(Car.class, id);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findById'"
      , JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return car;
  }

  public void create(Car car) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.save(car);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on insert ", JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void update(Car car, Integer id) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.update(car);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on update",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void delete(Car car) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.delete(car);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on delete",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public Car fidByModel(String model) throws SQLException {
    Session session = null;
    Car car = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      car = (Car) session.load(Car.class, model);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findById'"
        , JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return car;
  }

  public Car findByProductionYear(String production_year) {
    Session session = null;
    Car car = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      car = (Car) session.load(Car.class, production_year);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findById'",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return car;
  }
}
