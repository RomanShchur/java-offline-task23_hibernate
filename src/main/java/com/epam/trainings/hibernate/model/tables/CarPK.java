package com.epam.trainings.hibernate.model.tables;

public class CarPK {
  int id;
  int manufacturerId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getManufacturerId() {
    return manufacturerId;
  }

  public void setManufacturerId(int manufacturerId) {
    this.manufacturerId = manufacturerId;
  }

  public CarPK() {
  }

}
