package com.epam.trainings.hibernate.model.DAO.DAOimpl;

import com.epam.trainings.hibernate.model.DAO.DestributorDao;
import com.epam.trainings.hibernate.model.databaseConnection.HibernateUtil;
import com.epam.trainings.hibernate.model.tables.Destributor;
import org.hibernate.Session;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DestributorDaoImplementation implements DestributorDao {
  public List<Destributor> findAll() throws SQLException {
    Session session = null;
    List destributors = new ArrayList<Destributor>();
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      destributors = session.createCriteria(Destributor.class).list();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findAll'",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return destributors;
  }

  public Destributor findById(Integer id) throws SQLException {
    Session session = null;
    Destributor destributor = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      destributor = (Destributor) session.load(Destributor.class, id);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findById'"
        , JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return destributor;
  }

  public void create(Destributor destributor) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.save(destributor);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on insert ", JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void update(Destributor destributor, Integer id) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.update(destributor);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on update",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void delete(Destributor destributor) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.delete(destributor);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on delete",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public Destributor fidByTitle(String title) throws SQLException {
    Session session = null;
    Destributor destributor = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      destributor = (Destributor) session.load(Destributor.class, title);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findByTitle'"
        , JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return destributor;
  }

  public Destributor findByCity(String city) {
    Session session = null;
    Destributor destributor = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      destributor = (Destributor) session.load(Destributor.class, city);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findByTitle'",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return destributor;
  }
}
