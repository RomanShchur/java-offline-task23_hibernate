package com.epam.trainings.hibernate.model.DAO.DAOimpl;

import com.epam.trainings.hibernate.model.DAO.UserDao;
import com.epam.trainings.hibernate.model.databaseConnection.HibernateUtil;
import com.epam.trainings.hibernate.model.tables.User;
import org.hibernate.Session;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImplementation implements UserDao {
  public List<User> findAll() throws SQLException {
    Session session = null;
    List users = new ArrayList<User>();
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      users = session.createCriteria(User.class).list();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findAll'",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return users;
  }

  public User findById(Integer id) throws SQLException {
    Session session = null;
    User user = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      user = (User) session.load(User.class, id);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findById'"
        , JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return user;
  }

  public void create(User user) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.save(user);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on insert ", JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void update(User user, Integer id) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.update(user);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on update",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void delete(User user) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.delete(user);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on delete",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public User fidByUsername(String username) throws SQLException {
    Session session = null;
    User user = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      user = (User) session.load(User.class, username);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findById'"
        , JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return user;
  }

  public User findByEmail(String email) throws SQLException {
    Session session = null;
    User user = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      user = (User) session.load(User.class, email);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findById'"
        , JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return user;
  }
}
