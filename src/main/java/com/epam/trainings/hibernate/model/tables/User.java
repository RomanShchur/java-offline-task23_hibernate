package com.epam.trainings.hibernate.model.tables;



import org.hibernate.annotations.Entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
//@Table(name = "car")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  int id;
  @Column(name = "username", nullable = false, length = 45)
  String username;
  @Column(name = "email", nullable = true, length = 45)
  String email;
  @Column(name = "password", nullable = false, length = 45)
  String password;

  public User() {

  }

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getUsername() {
    return username;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }

  @Override public String toString() {
    return "User{" + "id=" + id + ", username='" + username + '\'' + ", email='"
      + email + '\'' + ", password='" + password + '\'' + '}';
  }
}
