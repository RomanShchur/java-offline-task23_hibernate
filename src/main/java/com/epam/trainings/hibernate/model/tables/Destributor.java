package com.epam.trainings.hibernate.model.tables;

import org.hibernate.annotations.Entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
//@Table(name = "destributor")
public class Destributor {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  int id;
  @Column(name = "title", nullable = false, length = 45)
  String title;
  @Column(name = "address", nullable = false, length = 45)
  String address;
  @Column(name = "phone", nullable = false, length = 45)
  String phone;
  @Column(name = "city", nullable = false, length = 45)
  String city;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public String getAddress() {
    return address;
  }
  public void setAddress(String address) {
    this.address = address;
  }
  public String getPhone() {
    return phone;
  }
  public void setPhone(String phone) {
    this.phone = phone;
  }
  public String getCity() {
    return city;
  }
  public void setCity(String city) {
    this.city = city;
  }

  @Override public String toString() {
    return "Destributor{" + "id=" + id + ", title='" + title + '\''
      + ", address='" + address + '\'' + ", phone='" + phone + '\'' + ", city='"
      + city + '\'' + '}';
  }
}
