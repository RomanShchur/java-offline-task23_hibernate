package com.epam.trainings.hibernate.model.tables;

import org.hibernate.annotations.Entity;
import org.jboss.logging.Field;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
//@Table(name = "car")
public class Manufacturer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  int id;
  @Column(name = "brand", nullable = false, length = 45)
  String brand;
  @Column(name = "country", nullable = false, length = 45)
  String country;

  public Manufacturer() {
  }

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getBrand() {
    return brand;
  }
  public void setBrand(String brand) {
    this.brand = brand;
  }
  public String getCountry() {
    return country;
  }
  public void setCountry(String country) {
    this.country = country;
  }

  @Override public String toString() {
    return "Manufacturer{" + "id=" + id + ", brand='" + brand + '\''
      + ", country='" + country + '\'' + '}';
  }
}
