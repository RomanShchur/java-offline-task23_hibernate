package com.epam.trainings.hibernate.model.DAO.DAOimpl;

import com.epam.trainings.hibernate.model.DAO.EngineDao;
import com.epam.trainings.hibernate.model.databaseConnection.HibernateUtil;
import com.epam.trainings.hibernate.model.tables.Engine;
import org.hibernate.Session;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EngineDaoImplementation implements EngineDao {
  public List<Engine> findAll() throws SQLException {
    Session session = null;
    List engines = new ArrayList<Engine>();
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      engines = session.createCriteria(Engine.class).list();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findAll'",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return engines;
  }

  public Engine findById(Integer id) throws SQLException {
    Session session = null;
    Engine engine = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      engine = (Engine) session.load(Engine.class, id);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findById'"
        , JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return engine;
  }

  public void create(Engine engine) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.save(engine);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on insert ", JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void update(Engine engine, Integer id) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.update(engine);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on update",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public void delete(Engine engine) throws SQLException {
    Session session = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      session.delete(engine);
      session.getTransaction().commit();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Error on delete",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
  }

  public Engine fidByFuelType(String fuel) throws SQLException {
    Session session = null;
    Engine engine = null;
    try {
      session = HibernateUtil.getSessionFactory().openSession();
      engine = (Engine) session.load(Engine.class, fuel);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "error 'findById'",
        JOptionPane.OK_OPTION);
    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
    }
    return engine;
  }
}
