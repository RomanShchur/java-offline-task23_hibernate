package com.epam.trainings.hibernate.model.DAO;


import com.epam.trainings.hibernate.model.tables.Car;

import java.sql.SQLException;
import java.util.List;

public interface CarDao extends MainDao<Car, Integer> {
  Car fidByModel(String model) throws SQLException;

  Car findByProductionYear(String productionYear) throws SQLException;
}
