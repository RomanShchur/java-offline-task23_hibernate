package com.epam.trainings.hibernate.model;

import com.epam.trainings.hibernate.model.DAO.DAOimpl.*;

public class Factory {
  private static CarDaoImplementation carDao = null;
  private static DestributorDaoImplementation destributorDao = null;
  private static EngineDaoImplementation engineDao = null;
  private static GearboxDaoImplementation gearboxDao = null;
  private static UsedCarDaoImplementation usedCarDao = null;
  private static UserDaoImplementation userDao = null;
  private static Factory instance = null;

  public static synchronized Factory getInstance(){
    if (instance == null){
      instance = new Factory();
    }
    return instance;
  }

  public CarDaoImplementation getCarDao(){
    if (carDao == null){
      carDao = new CarDaoImplementation();
    }
    return carDao;
  }

  public DestributorDaoImplementation getDestributorDao(){
    if (destributorDao == null){
      destributorDao = new DestributorDaoImplementation();
    }
    return destributorDao;
  }

  public EngineDaoImplementation getEngineDao(){
    if (engineDao == null){
      engineDao = new EngineDaoImplementation();
    }
    return engineDao;
  }

  public GearboxDaoImplementation getGearboxDao(){
    if (gearboxDao == null){
      gearboxDao = new GearboxDaoImplementation();
    }
    return gearboxDao;
  }

  public UsedCarDaoImplementation getUsedCarDao(){
    if (usedCarDao == null){
      usedCarDao = new UsedCarDaoImplementation();
    }
    return usedCarDao;
  }

  public UserDaoImplementation getUserDao(){
    if (userDao == null){
      userDao = new UserDaoImplementation();
    }
    return userDao;
  }
}
