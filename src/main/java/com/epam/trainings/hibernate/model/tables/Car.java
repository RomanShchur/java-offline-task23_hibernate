package com.epam.trainings.hibernate.model.tables;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Table;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@DynamicUpdate
@Table(appliesTo = "car")
@IdClass(CarPK.class)
public class Car implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  int id;
  @Column(name = "model", nullable = false, length = 45)
  String model;
  @Column(name = "production_year", nullable = false, length = 4)
  String productionYear;
  @Column(name = "length", nullable = false)
  int length;
  @Column(name = "width", nullable = false)
  int width;
  @Column(name = "height", nullable = false)
  int heigth;
  @Column(name = "seats", nullable = false)
  int seats;
  @Column(name = "doors", nullable = false)
  int doors;
  @Column(name = "weight", nullable = false)
  int weight;
  @Column(name = "fullWeight", nullable = false)
  int fullWeight;
  @Id
  @Column(name = "manufacturer_id", nullable = false)
  int manufacturerId;

  public Car() {
  }

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getModel() {
    return model;
  }
  public void setModel(String model) {
    this.model = model;
  }
  public String getProductionYear() {
    return productionYear;
  }
  public void setProductionYear(String productionYear) {
    this.productionYear = productionYear;
  }
  public int getLength() {
    return length;
  }
  public void setLength(int length) {
    this.length = length;
  }
  public int getWidth() {
    return width;
  }
  public void setWidth(int width) {
    this.width = width;
  }
  public int getHeigth() {
    return heigth;
  }
  public void setHeigth(int heigth) {
    this.heigth = heigth;
  }
  public int getSeats() {
    return seats;
  }
  public void setSeats(int seats) {
    this.seats = seats;
  }
  public int getDoors() {
    return doors;
  }
  public void setDoors(int doors) {
    this.doors = doors;
  }
  public int getWeight() {
    return weight;
  }
  public void setWeight(int weight) {
    this.weight = weight;
  }
  public int getFullWeight() {
    return fullWeight;
  }
  public void setFullWeight(int fullWeight) {
    this.fullWeight = fullWeight;
  }
  public int getManufacturerId() {
    return manufacturerId;
  }
  public void setManufacturerId(int manufacturerId) {
    this.manufacturerId = manufacturerId;
  }

  @Override public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Car))
      return false;
    Car car = (Car) o;
    return getId() == car.getId() && getLength() == car.getLength()
      && getWidth() == car.getWidth() && getHeigth() == car.getHeigth()
      && getSeats() == car.getSeats() && getDoors() == car.getDoors()
      && getWeight() == car.getWeight() && getFullWeight() == car
      .getFullWeight() && getManufacturerId() == car.getManufacturerId()
      && getModel().equals(car.getModel()) && getProductionYear()
      .equals(car.getProductionYear());
  }

  @Override public int hashCode() {
    return Objects
      .hash(getId(), getModel(), getProductionYear(), getLength(), getWidth(),
        getHeigth(), getSeats(), getDoors(), getWeight(), getFullWeight(),
        getManufacturerId());
  }

  @Override public String toString() {
    return "Car{" + "id=" + id + ", model='" + model + '\'' + ", UsedCar="
      + ", productionYear='" + productionYear + '\'' + ", length="
      + length + ", width=" + width + ", heigth=" + heigth + ", seats=" + seats
      + ", doors=" + doors + ", weight=" + weight + ", fullWeight=" + fullWeight
      + ", manufacturerId=" + manufacturerId + '}';
  }
}
