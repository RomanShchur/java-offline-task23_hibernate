package com.epam.trainings.hibernate.model.DAO;

import com.epam.trainings.hibernate.model.tables.UsedCar;

public interface UsedCarDao extends MainDao<UsedCar, Integer> {

}
