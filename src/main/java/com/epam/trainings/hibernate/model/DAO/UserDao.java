package com.epam.trainings.hibernate.model.DAO;

import com.epam.trainings.hibernate.model.tables.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDao extends MainDao<User, Integer> {
  User fidByUsername(String username) throws SQLException;

  User findByEmail(String email) throws SQLException;
}
