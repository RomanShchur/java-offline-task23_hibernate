package com.epam.trainings.hibernate.model.tables;

import org.hibernate.annotations.Entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
//@Table(name = "engine")
public class Engine {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  int id;
  @Column(name = "fuel", nullable = false, length = 45)
  String fuel;
  @Column(name = "volume", nullable = false)
  double volume;
  @Column(name = "cylinders", nullable = false)
  int cylinders;
  @Column(name = "valves", nullable = false)
  int valves;
  @Column(name = "power", nullable = false)
  int power;
  @Column(name = "maximum_speed", nullable = false)
  int maxSpeed;
  @Column(name = "fuel_consumption", nullable = false)
  double fuelConsumption;
  @Id
  @Column(name = "car_car_stats_id", nullable = false)
  int carCarStats;
  @Id
  @Column(name = "car_id", nullable = false)
  int carID;
  @Id
  @Column(name = "carManufacturerId", nullable = false)
  int carManufacturerId;

  public Engine() {
  }

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getFuel() {
    return fuel;
  }
  public void setFuel(String fuel) {
    this.fuel = fuel;
  }
  public double getVolume() {
    return volume;
  }
  public void setVolume(double volume) {
    this.volume = volume;
  }
  public int getCylinders() {
    return cylinders;
  }
  public void setCylinders(int cylinders) {
    this.cylinders = cylinders;
  }
  public int getValves() {
    return valves;
  }
  public void setValves(int valves) {
    this.valves = valves;
  }
  public int getPower() {
    return power;
  }
  public void setPower(int power) {
    this.power = power;
  }
  public int getMaxSpeed() {
    return maxSpeed;
  }
  public void setMaxSpeed(int maxSpeed) {
    this.maxSpeed = maxSpeed;
  }
  public double getFuelConsumption() {
    return fuelConsumption;
  }
  public void setFuelConsumption(double fuelConsumption) {
    this.fuelConsumption = fuelConsumption;
  }
  public int getCarID() {
    return carID;
  }
  public void setCarID(int carID) {
    this.carID = carID;
  }
  public int getCarManufacturerId() {
    return carManufacturerId;
  }
  public void setCarManufacturerId(int carManufacturerId) {
    this.carManufacturerId = carManufacturerId;
  }
  public int getCarCarStats() {
    return carCarStats;
  }
  public void setCarCarStats(int carCarStats) {
    this.carCarStats = carCarStats;
  }

  @Override public String toString() {
    return "Engine{" + "id=" + id + ", fuel='" + fuel + '\'' + ", volume="
      + volume + ", cylinders=" + cylinders + ", valves=" + valves + ", power="
      + power + ", maxSpeed=" + maxSpeed + ", fuelConsumption="
      + fuelConsumption + ", carID=" + carID + ", carManufacturerId="
      + carManufacturerId + '}';
  }
}
