package com.epam.trainings.hibernate.model.DAO;

import com.epam.trainings.hibernate.model.tables.Gearbox;

import java.sql.SQLException;
import java.util.List;

public interface GearboxDao extends MainDao<Gearbox, Integer> {
  List<Gearbox> fidIsMechanical(Boolean is_mechanical) throws SQLException;
}
