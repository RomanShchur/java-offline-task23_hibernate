package com.epam.trainings.hibernate.model.tables;


import org.hibernate.annotations.Entity;
import org.hibernate.annotations.Table;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Table(appliesTo = "book")
public class EngineGearbox {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "engine_id", nullable = false)
  int engineId;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "gearbox_id", nullable = false)
  int gearboxId;

  public EngineGearbox() {
  }

  public int getEngineId() {
    return engineId;
  }
  public void setEngineId(int engineId) {
    this.engineId = engineId;
  }
  public int getGearboxId() {
    return gearboxId;
  }
  public void setGearboxId(int gearboxId) {
    this.gearboxId = gearboxId;
  }

  @Override public String toString() {
    return "EngineGearbox{" + "engineId=" + engineId + ", gearboxId="
      + gearboxId + '}';
  }
}
