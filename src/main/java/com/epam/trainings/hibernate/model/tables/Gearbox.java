package com.epam.trainings.hibernate.model.tables;


import org.hibernate.annotations.Entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
//@Table(name = "car")
public class Gearbox {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  int id;
  @Column(name = "gearbox_type", nullable = false, length = 45)
  String gearboxType;
  @Column(name = "is_mechanical", nullable = false, length = 45)
  boolean isMechanical;
  @Column(name = "gear_number", nullable = false, length = 45)
  int gearNumber;
  @Column(name = "wheel_drive", nullable = false, length = 45)
  String wheelDrive;

  public Gearbox() {
  }

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getGearboxType() {
    return gearboxType;
  }
  public void setGearboxType(String gearboxType) {
    this.gearboxType = gearboxType;
  }
  public boolean isMechanical() {
    return isMechanical;
  }
  public void setMechanical(boolean mechanical) {
    isMechanical = mechanical;
  }
  public int getGearNumber() {
    return gearNumber;
  }
  public void setGearNumber(int gearNumber) {
    this.gearNumber = gearNumber;
  }
  public String getWheelDrive() {
    return wheelDrive;
  }
  public void setWheelDrive(String wheelDrive) {
    this.wheelDrive = wheelDrive;
  }

  @Override public String toString() {
    return "Gearbox{" + "id=" + id + ", gearboxType='" + gearboxType + '\''
      + ", isMechanical=" + isMechanical + ", gearNumber=" + gearNumber
      + ", wheelDrive='" + wheelDrive + '\'' + '}';
  }
}
