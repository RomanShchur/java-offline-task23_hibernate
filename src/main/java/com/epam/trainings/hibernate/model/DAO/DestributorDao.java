package com.epam.trainings.hibernate.model.DAO;


import com.epam.trainings.hibernate.model.tables.Destributor;

import java.sql.SQLException;
import java.util.List;

public interface DestributorDao extends MainDao<Destributor, Integer> {
  Destributor fidByTitle(String title) throws SQLException;

  Destributor findByCity(String city) throws SQLException;
}
